#include <iostream>
#pragma once

template <class T>

class Stack
{
private:
	int size = 0;
	T a = 0;
	T* array = new (std::nothrow) T[size];
	
public:

	Stack()
	{
		std::cout << "\n" << "How many elements do you want to input?" << "\n";
		std::cin >> size;

		std::cout << "\n" << "Input them" << "\n";
		array = new (std::nothrow) T[size];
		for (int i = 0; i < size; i++)
		{
			std::cin >> array[i];
		}
	}

	~Stack()
	{
		delete[] array;
	}

	void print()
	{
		std::cout << "\n" << "Array: ";
		for (int i = 0; i < size; i++)
		{
			std::cout << array[i] << " ";
		}
		std::cout << "\n";
	}

	void push()
	{
		T* copy = new (std::nothrow) T[size+1];

		for (int i = 0; i < size; i++)
		{
			copy[i] = array[i];
		}
		delete[] array;

		size++;
		
		std::cout << "\n" << "Input one more stack element" << "\n";
		std::cin >> a;
		copy[size-1] = a;

		array = new (std::nothrow) T[size];
		for (int i = 0; i < size; i++)
		{
			array[i] = copy[i];
		}
		delete[] copy;
	}

	void pop()
	{
		size--;

		T* copy = new (std::nothrow) T[size];
		for (int i = 0; i < size; i++)
		{
			copy[i] = array[i];
		}
		delete[] array;
		
		array = new (std::nothrow) T[size];
		for (int i = 0; i < size; i++)
		{
			array[i] = copy[i];
		}
		delete[] copy;
	}

	void top()
	{
		std::cout << "\n" << "Top element: " << array[size-1] << "\n";
	}

	void empty()
	{
		if (size == 0)
		{
			std::cout << "\n" << "Stack is empty" << "\n";
		}
		else
		{
			std::cout << "\n" << "Stack is not empty" << "\n";
		}
	}

	void peek()
	{
		std::cout << "\n" << "What element of the stack do you want to see?" << "\n";

		int n;
		std::cin >> n;

		if (n <= size)
		{
			std::cout << "\n" << n << " element of the stack: " << array[n - 1] << "\n";
		}
		else
		{
			std::cout << "\n" << "This element does not exist" << "\n";
			peek();
		}
	}
};