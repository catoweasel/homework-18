#include <iostream>
#include "StackClass.h"
#include <string>

std::string whattodo()
{
    std::cout << "\n" << "What do you want to do with the stack?" << "\n";
    std::cout << "push\t" << "pop\t" << "top\t" << "peek\n\n";

    std::cin.ignore(555, '\n');
    std::string operation;
    std::cin >> operation;

    return (operation);
}

int main()
{
    setlocale(LC_ALL, "rus");
    
    std::cout << "\t\t" << "Welcome to the stack" << "\n\n";

    std::cout << "What values do you want to work with?" << "\n\n";
    std::cout << "For integers press 1" << "\n";
    std::cout << "For floats press 2" << "\n";
    std::cout << "For doubles press 3" << "\n";
    std::cout << "For chars press 4" << "\n";

    int press = 0;
    std::cin >> press;

    switch (press)
    {
        case 1: 
        {
            std::cout << "\t\t" << "Stack of integers" << "\n\n";

            Stack<int> integer;
            integer.empty();
            integer.print();

            for (int i = 0; i < 5; i++)
            {
                std::string operation = whattodo();

                if (operation == "push")
                {
                    integer.push();
                    integer.print();
                }
                else if (operation == "pop")
                {
                    integer.pop();
                    integer.print();
                }
                else if (operation == "top")
                {
                    integer.top();
                }
                else if (operation == "peek")
                {
                    integer.peek();
                }
            }
            std::cout << "\n" << "Thank you for using stack!" << "\n";
            break;
        }

        case 2: 
        {
            std::cout << "\t\t" << "Stack of floats" << "\n\n";

            Stack<float> fractional;
            fractional.empty();
            fractional.print();

            for (int i = 0; i < 5; i++)
            {
                std::string operation = whattodo();

                if (operation == "push")
                {
                    fractional.push();
                    fractional.print();
                }
                else if (operation == "pop")
                {
                    fractional.pop();
                    fractional.print();
                }
                else if (operation == "top")
                {
                    fractional.top();
                }
                else if (operation == "peek")
                {
                    fractional.peek();
                }
            }
            std::cout << "\n" << "Thank you for using stack!" << "\n";
            break;
        }

        case 3: 
        {
            std::cout << "\t\t" << "Stack of doubles" << "\n\n";

            Stack<double> longfractional;
            longfractional.empty();
            longfractional.print();

            for (int i = 0; i < 5; i++)
            {
                std::string operation = whattodo();

                if (operation == "push")
                {
                    longfractional.push();
                    longfractional.print();
                }
                else if (operation == "pop")
                {
                    longfractional.pop();
                    longfractional.print();
                }
                else if (operation == "top")
                {
                    longfractional.top();
                }
                else if (operation == "peek")
                {
                    longfractional.peek();
                }
            }
            std::cout << "\n" << "Thank you for using stack!" << "\n";
            break;
        }

        case 4: 
        {
            std::cout << "\t\t" << "Stack of chars" << "\n\n";

            Stack<char> symbol;
            symbol.empty();
            symbol.print();

            for (int i = 0; i < 5; i++)
            {
                std::string operation = whattodo();

                if (operation == "push")
                {
                    symbol.push();
                    symbol.print();
                }
                else if (operation == "pop")
                {
                    symbol.pop();
                    symbol.print();
                }
                else if (operation == "top")
                {
                    symbol.top();
                }
                else if (operation == "peek")
                {
                    symbol.peek();
                }
            }
            std::cout << "\n" << "Thank you for using stack!" << "\n";
            break;
        }

        default:
            std::cout << "\n" << "Input Error. Please try again" << "\n\n";
            main();

    }

    return 0;
}
